package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;

/**
 * Servlet implementation class TinyGramTimeline
 */
@SuppressWarnings("serial")
public class TinyGramTimeline extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TinyGramTimeline() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		

		String user=req.getParameter("user");
		if (user==null) {user="u416";}
		resp.getWriter().println("getting timeline for:"+user);

		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		Filter pf = new FilterPredicate("followers", FilterOperator.EQUAL,user);
		Query q = new Query("Message").setFilter(pf);

		PreparedQuery pq = ds.prepare(q);
		
		long t1=System.currentTimeMillis();
		
		List<Entity> results = pq.asList(FetchOptions.Builder.withLimit(100));

		resp.getWriter().println("<h1>query</h1>");

		for (Entity r : results) {
			resp.getWriter().println("<li>:" + r.getProperty("body"));
		}

		resp.getWriter().println("done in "+(System.currentTimeMillis()-t1));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
