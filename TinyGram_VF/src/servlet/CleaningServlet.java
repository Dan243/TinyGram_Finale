package servlet;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tinygramvf.entities.Message;
import fr.tinygramvf.entities.MessageIndex;
import fr.tinygramvf.entities.PMF;
import fr.tinygramvf.entities.User;


public class CleaningServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
      
	  processRequest(request, response);

  }

private void processRequest(HttpServletRequest request, HttpServletResponse response) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		//CLEANING PART pour l'instant nettoie que Personne et Post
		
		// faut add PersonIndex
		Query query = pm.newQuery(User.class);
		query.deletePersistentAll();
		Query query2 = pm.newQuery(Message.class);
		query2.deletePersistentAll();
		Query query3 = pm.newQuery(MessageIndex.class);
		query3.deletePersistentAll();

	
	}
}
