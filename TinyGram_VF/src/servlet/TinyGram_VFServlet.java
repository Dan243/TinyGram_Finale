package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

/**
 * Servlet implementation class TinyGramServlet
 */

public class TinyGram_VFServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TinyGram_VFServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("populating store...");
		
		Random r=new Random();


		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		int maxmessage=250;
		int maxuser=1000;
		for (int i = 0; i < maxmessage; i++) {
			Entity e = new Entity("Message", "m" + i);
			e.setProperty("body", "Hello " + i );
			ArrayList<String> followers = new ArrayList<String>();
			for (int j = 0; j < 100; j++) {
				followers.add("u"+r.nextInt(maxuser+1));
			}
			e.setProperty("followers", followers);
			datastore.put(e);
			resp.getWriter().println("wrote:"+e.getKey());
		}
		resp.getWriter().println("done");
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
